""" A collection of plotting classes derived from PlotBase """
"""           Copyright Jackson Burzynski 2021             """

import ROOT
import atlas_style

import os
import math
from sys import argv, exit

from plot_base import *
from plot_util import *
from PlotObject import PlotObject

ROOT.gROOT.SetBatch(ROOT.kTRUE)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetHatchesLineWidth(1)


class Hist1D(PlotBase):
    """
    Class for basic 1D histogram plots
    """
    def __init__(self, 
                 hists, 
                 y_axis_type = "Events", 
                 rebin       = False,
                 norm        = False,
                 normWidth   = False,
                 **kwargs):
        """
        Args:
            hists (:obj:`list'): A list of PlotObjects
        """
        super(Hist1D, self).__init__(
                legend_loc      = [0.6,0.9,0.9, 0.9 - 0.045*(len(hists)) ],
                atlas_loc       = [0.17,0.875],
                extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.hists = hists

        self.rebin = rebin
        self.norm  = norm
        self.normWidth = normWidth

        # perform operations needed on each PlotObject
        # eg: rebin, normalize, format 


        for h in hists:
            if self.rebin != None:
                if isinstance(self.rebin, list):
                    import array
                    xbins = array.array('d', self.rebin)
                    h.obj = h.obj.Rebin(len(xbins)-1, h.obj.GetName() + "rebin", xbins)
                else:
                    h.obj = h.obj.Rebin(self.rebin, h.obj.GetName() + "rebin")

            if self.norm == True :
                h.obj.Scale(1.0/h.obj.Integral())
            if self.normWidth == True :
                h.obj.Scale(1.0, "width")

            self.set_x_axis_bounds(h.obj)
            self.set_y_min(h.obj)

            self.set_titles(h.obj, y_axis_type)
            h.format_for_drawing()

        if not self.y_min:
          self.y_min = max(0.0001, min(map(lambda h: h.GetMinimum(), [h.obj for h in self.hists]))) / 10.

        for h in hists:
            self.set_y_min(h.obj)

        # set y_max based on range of given plots
        self.pad_empty_space([h.obj for h in self.hists])

    def draw(self):

        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        draw_hists(self.hists)

        hist_errors = []
        for h in self.hists:

            if("hist" not in h.style.lower()):
                continue
            herr = h.obj.Clone(h.obj.GetName() + "err")
            herr.SetFillColorAlpha(herr.GetLineColor(),0.35)
            herr.SetFillStyle(1001)
            herr.SetMarkerSize(0)
            herr_obj = PlotObject( obj = herr )
            hist_errors.append(herr_obj)
        draw_hists(hist_errors, "E2")

        for h, herr in zip(self.hists, hist_errors):
          if 'pe' in h.style:
            self.leg.AddEntry(h.obj, h.legend, 'pe')
          else:
            self.leg.AddEntry(herr.obj, h.legend, 'lf')

        ROOT.gPad.RedrawAxis()
        self.print_to_file(self.name + ".pdf")
        pad1.Close()

    def draw_ratio(self, nums, den,
                   draw_ratio  = False,
                   ratio_line  = 1.0,
                   ratio_min   = 0.4,
                   ratio_max   = 1.6,
                   ratio_label = "ratio"):

        ratios = []
        
        den.obj.Sumw2()
        for num in nums:
          num.obj.Sumw2()
          r = num.obj.Clone("ratio"+num.legend)
          r.Divide(den.obj)
          ratio = PlotObject(obj = r, color = num.color, line = num.line_style, marker = num.marker_style, style = num.style)
          ratio.obj.GetYaxis().SetTitle(ratio_label)
          ratio.format_for_drawing_sub()
          ratios.append(ratio)


        pad1, pad2 = format_2pads_for_ratio()

        pad1.Draw()
        pad2.Draw()

        pad2.cd()
        for ratio in ratios:
          ratio.obj.SetMinimum(ratio_min)
          ratio.obj.SetMaximum(ratio_max)
          ratio.obj.SetMarkerSize(1.0)
          ratio.obj.Draw("hist PE same")

        line = ROOT.TLine(self.x_min,ratio_line,self.x_max,ratio_line)
        line.SetLineColor(1)
        line.SetLineWidth(1)
        line.SetLineStyle(2)
        line.Draw("same")

        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        draw_hists(self.hists)
        hist_errors = []
        for h in self.hists:
            if("hist" not in h.style.lower()):
                continue
            herr = h.obj.Clone(h.obj.GetName() + "err")
            herr.SetFillColorAlpha(herr.GetLineColor(),0.35)
            herr.SetFillStyle(1001)
            herr.SetMarkerSize(0)
            herr_obj = PlotObject( obj = herr )
            hist_errors.append(herr_obj)
        draw_hists(hist_errors, "E2")

        for h, herr in zip(self.hists, hist_errors):
          if 'pe' in h.style:
            self.leg.AddEntry(h.obj, h.legend, 'pe')
          else:
            self.leg.AddEntry(herr.obj, h.legend, 'lf')


        ROOT.gPad.RedrawAxis()
        self.print_to_file(self.name + "ratio.pdf")
        pad1.Close()
        pad2.Close()


class Hist2D(PlotBase):
    def __init__(self, hist, text=False, profile=False,**kwargs):

        super(Hist2D, self).__init__(
                legend_loc = [0.6,0.9,0.85, 0.9 ],
                atlas_loc = [0.175,0.95],
                **kwargs)

        self.hist = hist

        self.text = text

        self.set_x_axis_bounds(self.hist.obj)
        self.set_y_axis_bounds(self.hist.obj)
        self.set_z_min(self.hist.obj)
        self.set_z_max(self.hist.obj)

        hist.obj.GetZaxis().SetMaxDigits(3);
        self.set_titles2D(self.hist.obj,"")

    def draw(self):

        pad1 = self.canvas.cd(1)

        self.hist.format_for_drawing()

        format_simple_pad_2D(pad1)
        pad1.cd()

        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()
        if (self.log_scale_z):
            pad1.SetLogz()
    
        if self.text:
          self.hist.obj.Draw("TEXTE colz")
        else:
          self.hist.obj.Draw("colz")

        self.print_to_file(self.name + ".pdf")
        pad1.Close()
