import ROOT

def format_simple_pad_graph(pad):
    pad.SetPad(0.0, 0.0, 1., 1.)
    pad.SetTopMargin(0.05)
    pad.SetRightMargin(0.03)
    pad.SetLeftMargin(0.12)
    pad.SetBottomMargin(0.17)
    pad.SetBorderSize(0)
    pad.SetGridy(0)
    pad.SetBorderSize(0)

def format_simple_pad(pad):
    pad.SetPad(0.0, 0.0, 1., 1.)
    pad.SetTopMargin(0.06)
    pad.SetRightMargin(0.05)
    pad.SetLeftMargin(0.12)
    pad.SetBottomMargin(0.15)
    pad.SetBorderSize(0)
    pad.SetGridy(0)
    pad.SetBorderSize(0)

def format_simple_pad_2D(pad):
    pad.SetPad(0.0, 0.0, 1., 1.)
    pad.SetTopMargin(0.12)
    pad.SetRightMargin(0.17)
    pad.SetLeftMargin(0.13)
    pad.SetBottomMargin(0.14)
    pad.SetBorderSize(0)
    pad.SetGridy(0)
    pad.SetBorderSize(0)

def format_2pads_for_ratio():
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.25, 1., 1.0)
    pad1.SetTopMargin(0.065)
    pad1.SetRightMargin(0.04)
    pad1.SetLeftMargin(0.13)
    pad1.SetBottomMargin(0.02)
    pad1.SetBorderSize(0)
    pad1.SetGridy(0)
    pad1.SetBorderSize(0)

    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.0, 1, 0.25)
    pad2.SetTopMargin(0.01)
    pad2.SetRightMargin(0.04)
    pad2.SetLeftMargin(0.13)
    pad2.SetBottomMargin(0.4)
    pad2.SetFillColorAlpha(0, 0.)
    pad2.SetBorderSize(0)
    pad2.SetGridy(0)
    pad2.SetBorderSize(0)

    return pad1, pad2

def draw_hists(hlist, options = None):
    assert(len(hlist) > 0)

    for h in hlist:
        if(options):
            h.obj.Draw(options + " same")
        else:
            h.obj.Draw(h.style + " same")
