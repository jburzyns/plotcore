import ROOT

class PlotObject():

    def __init__(self, sample = None, files = None, path = None, obj = None, color = None, marker = None, line = None, legend = None, style = None):

        if obj is None:
          self.obj = None
          for f in files:
              if not self.obj:
                  self.obj = f.Get(path).Clone( path.split('/')[-1] + sample)
              else:
                  self.obj += f.Get(path).Clone( path.split('/')[-1] + sample) 
        else:
          self.obj = obj

        self.legend       = legend
        self.color        = color
        self.line_style   = line
        self.marker_style = marker
        self.style        = style

    def format_for_drawing(self):

        self.obj.SetLineColor   ( ROOT.TColor.GetColor(self.color) )
        self.obj.SetLineStyle   ( self.line_style   )

        self.obj.SetMarkerStyle ( self.marker_style )
        self.obj.SetMarkerColor ( ROOT.TColor.GetColor(self.color) )

        self.obj.GetXaxis().SetTitleOffset(1.25)
        self.obj.GetYaxis().SetTitleOffset(1.15)
        self.obj.GetXaxis().SetTitleSize(.05)
        self.obj.GetYaxis().SetTitleSize(.05)
        self.obj.GetXaxis().SetLabelSize(.05)
        self.obj.GetYaxis().SetLabelSize(.05)
        self.obj.GetXaxis().SetLabelOffset(0.015)
        self.obj.GetZaxis().SetTitleOffset(1.15)
        self.obj.GetYaxis().SetMaxDigits(3)

    def format_for_drawing_sub(self):

        self.obj.SetLineColor   ( ROOT.TColor.GetColor(self.color) )
        self.obj.SetLineStyle   ( self.line_style   )

        self.obj.SetMarkerStyle ( self.marker_style )
        self.obj.SetMarkerColor ( ROOT.TColor.GetColor(self.color) )

        self.obj.GetXaxis().SetTitleOffset(1.3);
        self.obj.GetYaxis().SetTitleOffset(.37)
        self.obj.GetXaxis().SetTitleSize(.14)
        self.obj.GetYaxis().SetTitleSize(.14)
        self.obj.GetXaxis().SetLabelSize(0.135)
        self.obj.GetXaxis().SetLabelOffset(0.03)
        self.obj.GetYaxis().SetLabelSize(0.125)
        self.obj.GetYaxis().SetNdivisions(505)
        self.obj.GetXaxis().SetLabelOffset(0.05)

