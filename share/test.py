#!/usr/bin/python
################################################
# plot_hists.py                                #
################################################
# code for making nice plots from output       #
# of the hist algo                             #
################################################
# Jackson Burzynski                            #
################################################



import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--inputFile', dest='inputFile', help='Name of the input text file containing the list of samples to process')
parser.add_argument('-o','--outputDir', dest='outputDir', help='Output directory for the plots')
parser.add_argument('--log', action='store_true', help='flag for plotting with log y-scale')
parser.add_argument('--norm', action='store_true', help='flag for normalizing all histograms to unit area')
args = parser.parse_args()

import ROOT
from PlotObject import PlotObject
from plot_data import *
from plot_classes import Hist1D, Hist2D

samples = []
# initialize empty plot dictionary
plots = {}

def parseSample(inFile):
  if "data" in inFile:
    return "data"

  DSID = inFile.split("mc16_13TeV")[1].split(".")[1]
  return DSID

def main():

  files = {}
  plotDict = {}

  # make sure to keep the files open in memory
  with open(args.inputFile) as fileList:
    for f in fileList:
      f = f.strip()

      sample = parseSample(f)
      files[sample] = ROOT.TFile(f)

      dir_keys = ROOT.gDirectory.GetListOfKeys()
      for dir_key in dir_keys:
        # go back to top level, then cd to cut dir
        files[sample].cd()

        if "Hist" not in dir_key.GetName():
          continue

        files[sample].cd(dir_key.GetName())

        var_keys = ROOT.gDirectory.GetListOfKeys()
        for var_key in var_keys:
          plot = PlotObject(sample, f = files[sample], name = dir_key.GetName() + "/" + var_key.GetName())

          if dir_key.GetName() + "__" + var_key.GetName() not in plotDict:
            plotDict[dir_key.GetName() + "__" + var_key.GetName()] = []
            plotDict[dir_key.GetName() + "__" + var_key.GetName()].append(plot)
          else:
            plotDict[dir_key.GetName() + "__" + var_key.GetName()].append(plot)

  for plotName in plotDict:

    var = plotName.split("__")[1]

    cut = plotName.split("__")[0].split("_")[1]

    extra_lines = cutDict[cut]

    if var in plotData1D:

      h = Hist1D(  hists              = plotDict[plotName],
                   name               = args.outputDir + "/" + plotName,
                   y_axis_type        = plotData1D[var]["yaxis"],
                   x_title            = plotData1D[var]["label"],
                   x_units            = plotData1D[var]["unit"],
                   rebin              = plotData1D[var]["rebin"],
                   log_scale_y        = args.log,
                   x_max              = plotData1D[var]["xmax"],
                   x_min              = plotData1D[var]["xmin"],
                   y_min              = 0.1 if not args.norm else 0.00001,
                   norm               = args.norm,
                   hide_lumi          = False,
                   lumi_val           = "139",
                   extra_legend_lines = extra_lines)

      h.draw()
      #h.draw_ratio(plotDict[plotName][0],plotDict[plotName][1],ratio_label = "Sig1/Sig2")

    elif var in plotData2D:
      for plot in plotDict[plotName]:

        h = Hist2D(  hist               = plot,
                     name               = args.outputDir + "/" + plot.sample + "_" + plotName,
                     z_title            = "Vertices",
                     x_title            = plotData2D[var]["x_label"],
                     x_units            = plotData2D[var]["x_unit"],
                     y_title            = plotData2D[var]["y_label"],
                     y_units            = plotData2D[var]["y_unit"],
                     rebinX             = plotData2D[var]["rebinX"],
                     rebinY             = plotData2D[var]["rebinY"],
                     x_max              = plotData2D[var]["xmax"],
                     y_max              = plotData2D[var]["ymax"],
                     hide_lumi          = False,
                     lumi_val           = "139",
                     extra_legend_lines = [])

        h.draw()

if __name__ == "__main__":
  main()

